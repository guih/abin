<?php 

	class Main_Model {

		private $title;
		private $db_instance;
		private $item_meta;

		private $code;
		private $description;
		private $price;
		private $amount;
		private $woner;
		private $year;
		private $month;
		private $day;


		public function __construct($id){
			$this->id = $id;
			$main_ctrl = new Main_Controller();
			$this->item_meta = $main_ctrl->fetch_item_by_id($this->id);			
		}
		public function __get( $property ){
			
			switch( $property ){
				case 'title':
					$this->treath_empty( 'title', $this->item_meta['title'] );
				break;
				case 'code':
					$this->treath_empty( 'code', $this->item_meta['code'] );
				break;				
				case 'description':
					$this->treath_empty( 'description', $this->item_meta['description'] );
				break;				
				case 'price':
					$this->treath_empty( 'price', $this->item_meta['price'] );
				break;				
				case 'amount':
					$this->treath_empty( 'amount', $this->item_meta['amount'] );
				break;				
				case 'owner':
					$this->treath_empty( 'owner', $this->item_meta['owner'] );
				break;				
				case 'year':
					$this->treath_empty( 'year', $this->item_meta['year'] );
				break;				
				case 'month':
					$this->treath_empty( 'month', $this->item_meta['month'] );
				break;
				case 'day':
					$this->treath_empty( 'day', $this->item_meta['day'] );
				break;
			}

			return $this->$property;
		}
		public function __set($property, $value){
			$this->$property = $value;
		}

		public function treath_empty($property, $newval){
			if( ! isset( $this->property ) )
				return $this->$property = $newval;
		}
	}
?>

