<?php 
	class User_Model {
		private $user;
		private $id;
		private $name;
		private $email;
		private $password;
		private $level;
		private $avatar;
		private $sells;


		public function __construct($id){
			$main_ctrl = new Main_Controller();
			$this->user = $main_ctrl->fetch_single_user($id );
			$this->sells = $main_ctrl->get_user_sells($id );
			// var_dump($this);
		}
		public function __get($what=''){	
			switch( $what ){
				case "id":
					return $this->treath_empty( $what, $this->user['id']);
					break;				
				case "name":
					return $this->treath_empty( $what, $this->user['name']);
					break;				
				case "email":
					return $this->treath_empty( $what, $this->user['email']);
					break;				
				case "password":
					return $this->treath_empty( $what, $this->user['password']);
					break;				
				case "level":
					return $this->treath_empty( $what, $this->user['level']);
					break;				
				case "avatar":
					$avatar = ( ! $this->user['avatar'] ) ? 'null' : $this->user['avatar'];
					return $this->treath_empty( $what, $avatar);
					break;
				case "sells":
					if( isset( $this->sells ) ) 
						return $this->sells;
					break;				
				default:
					
					break;
			}
		}
		public function treath_empty($item, $val){
			if( ! isset( $this->$item ) )
				return $this->$item = $val;
		}
	}

?>
