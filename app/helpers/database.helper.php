<?php
	class Database_Helper{

		private $connection = null;

		const DB_USER = 'root';
		const DB_PASS = '';
		const DB_DATA = 'noway';
		const DB_HOST = 'localhost';

		public function __construct($info='')
		{
			$this->_connect();
		}

		public function _connect()		
		{
			try{
				$this->connection = new PDO(
					"mysql:host=".self::DB_HOST.";
					dbname=".self::DB_DATA
					,self::DB_USER
					,self::DB_PASS
				);
				$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
			}catch( PDOException $e  ){
				die('Error while trying connect database!');
			}

			if( $this->connection != null )
				return $this->connection;
		}
		public function what_type($value)
		{
			if( is_bool($value) ) {
				return PDO::PARAM_BOOL;
			} elseif( is_numeric($value) ) {
				return PDO::PARAM_INT;
			} elseif( empty($value) ) {
				return PDO::PARAM_NULL;
			} else {
				return PDO::PARAM_STR;
			}

		}
		public function _fetch_user($value='')
		{
			$sql = "SELECT * FROM users where name = '".$value."'";
			$user = $this->connection->query($sql)->fetch(PDO::FETCH_ASSOC);
			return $user;
		}
		public function _fetch_user_by_id($id='')
		{
			$sql = "SELECT * FROM users where id = '".$id."'";
			$user = $this->connection->query($sql)->fetch(PDO::FETCH_ASSOC);
			return $user;
		}
		public function _fetch_users($value='')
		{
			$sql = "SELECT * FROM users";
			$users = $this->connection->query($sql)->fetchAll(PDO::FETCH_ASSOC);
			return $users;
		}

		public function _fetch_product_by_id($id='')
		{
			$sql = "SELECT * FROM products where id = '".$id."'";
			$item = $this->connection->query($sql)->fetch(PDO::FETCH_ASSOC);
			return $item;
		}
		public function _fetch_clients()
		{
			$sql = "SELECT * FROM clients";
			$items = $this->connection->query($sql)->fetchAll(PDO::FETCH_ASSOC);
			return $items;
		}
		public function _fetch_client_by_id($id='')
		{
			$sql = "SELECT * FROM clients where id = '".$id."'";
			$client = $this->connection->query($sql)->fetch(PDO::FETCH_ASSOC);
			return $client;
		}
		public function _fetch_sell_between_date($month, $year)
		{
			$sql = "SELECT * FROM sells where month = '".$month."' and year = '".$year."';";
			$item = $this->connection->query($sql)->fetchAll(PDO::FETCH_ASSOC);
			return $item;
		}
		public function _fetch_sells_by_user($id)
		{
			$sql = "SELECT *
					FROM `sells`
					WHERE owner = {$id};";
			$item = $this->connection->query($sql)->fetchAll(PDO::FETCH_ASSOC);
			return $item;
		}

		public function _update_item($id, $data){
			$stmt = $this->connection->prepare('UPDATE products SET amount = :amount WHERE id = :id');
			$stmt->execute(array(
				':id'   => $id,
				':amount' => $data
			));
		}
		public function _update_item_prop($key, $data){
			$id = $data['id'];

			if( $key !== 'id' ){
				$stmt = $this->connection->prepare('UPDATE products SET '.$key.' = :'.$key.' WHERE id = :id');
				$stmt->execute(array(
					':id'   => $id,
					':'.$key => $data[$key]
				));
			}

		}
		public function _update_user_prop($key, $data){
			$id = $data['id'];
		
			if( $key == "password" ){
				$user_meta = $this->_fetch_user_by_id( $id );
				$password = "";

				if( $data['password'] == "" ){
					$password = $user_meta['password'];
				}else{
					$password = @md5( $data['password'] );
				}

				$data['password'] = $password;
			}

			if( $key !== 'id' ){
		
				$stmt = $this->connection->prepare('UPDATE users SET '.$key.' = :'.$key.' WHERE id = :id');
				$stmt->execute(array(
					':id'   => $id,
					':'.$key => $data[$key]
				));
				// var_dump($stmt,$key,$data, $data[$key]);
			}

		}
		public function _fetch_value_of_sells()
		{
			$sql = "SELECT `value`, `month`
				FROM `sells`
				WHERE 1 ORDER BY `sells`.`timex` DESC";

			$sells = $this->connection->query($sql)->fetchAll(PDO::FETCH_ASSOC);
			return $sells;
		}

		public function _insert($table, $data){

			$fields       = array(); 
			$params       = array(); 
			$placeholders = array();

	        foreach($data as $field => $value) {
				$fields[]       = "`".$field."`";
				$params[]       = $value;
				$placeholders[] = "?";
	        }
	       
			$sql = "INSERT INTO `".$table."`".
					" (".implode(",",$fields).") VALUES (".implode(",",$placeholders).")";
	       
			$stmt = $this->connection->prepare($sql);
			return $stmt->execute($params);

		}
		public function _fetch_data(){
			$sqlSelect = "SELECT * from `products`";
			$data = $this->connection->query($sqlSelect)->fetchAll(PDO::FETCH_ASSOC);

			return $data;
		}
		public function _fetch_sells_only(){
			$sqlSelect = "SELECT * from `sells` where 1 order by `sells`.`timex` DESC ";
			$data = $this->connection->query($sqlSelect)->fetchAll(PDO::FETCH_ASSOC);

			return $data;
		}

	}

?>