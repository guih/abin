<?php  
	include "database.helper.php";


	class Users_Helper{

		private $db;

		public function __construct()
		{
			$this->db = new Database_Helper();
		}
		public function add_user($where , $data)
		{
			$this->db->_insert( $where, $data );
		}
		public function match_user( $user='' )
		{
			return $this->db->_fetch_user( $user );
		}
		public function single_user($id){
			return $this->db->_fetch_user_by_id($id);
		}
		public function log_user($user_meta=array(), $given_c){
			// var_dump($user_meta, $given_c);

			$user_password = @md5( $given_c[1] );

			if( $user_password == $user_meta['password'] ){
				$this->give_this_user_a_session($user_meta);
			}else{
				die( 
					json_encode( 
						array(
						'error' => 'Invalid password.',
						'type'  => 'invalid_password'  
						) 
					) 
				);				
			}
		}
		public function give_this_user_a_session($user_meta){
			$_SESSION['user_name']  = $user_meta['name'];
			$_SESSION['user_id']    = $user_meta['id'];
			$_SESSION['user_token'] = @md5( $user_meta['name'] . mktime() . $user_meta['id'] ); 
			die( json_encode( array('success' => 1 ) ) );

			// $_SESSION[$user_meta['id']] = $user_meta['name'];			
		}
		public function logg_out(){
			session_destroy();
			header('Location: ?');
		}
		public function update_user_single($key, $data){
			$data = $this->db->_update_user_prop($key, $data);
			return $data;
		}
		public function get_pdo_handler(){
			return $this->db;
		}
		public function get_users(){
			return $this->db->_fetch_users();
		}
		public function fetch_user_sells($id){
			return $this->db->_fetch_sells_by_user( $id );
		}
	}
//  $args = array_values($data);
// array_unshift($args, $sql);
// call_user_func_array('query', $args); 
?>

