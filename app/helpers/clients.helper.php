<?php 

	class Clients_Helper{
		private $db;
		public function __construct(){
			$this->db = new Database_Helper();
		}
		public function add_client($where='clients', $meta){
			return $this->db->_insert( $where, $meta );
		}
		public function get_clients(){
			return $this->db->_fetch_clients();
		}
		public function get_client_by_id($id){
			return $this->db->_fetch_client_by_id( $id );
		}
	}
?>