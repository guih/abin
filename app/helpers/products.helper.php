<?php
	// include "database.helper.php";

	class Products_Helper{
		private $db;

		public function __construct( $db_instance )
		{
			$this->db = $db_instance;
		}

		public function insert_product($data=array()){

			$item_meta                = array();
			$item_meta['title']       = $data['item_title'];
			$item_meta['code']        = ( empty( $data['item_code'] ) ? hash('adler32', time(), false) : $data['item_code'] );			
			$item_meta['description'] = $data['item_description'];			
			$item_meta['price']       = $data['item_price'];
			$item_meta['amount']      = $data['item_amount'];
			$item_meta['owner']       = $_SESSION['user_id'];
			$item_meta['year']        = date("Y");
			$item_meta['month']       = date("m");
			$item_meta['day']         = date("d");

			if( $this->db->_insert( 'products', $item_meta ) ){
				die( json_encode( array( 'success' => 'Product saved!' ) ) );
			}

		}
		public function sell($data=array()){

			$item_meta              = array();
			$item_meta['parent_id'] = $data['item'];
			$item_meta['amount']    = $data['item_amount'];			
			$item_meta['acre']      = $data['item_acre'];			
			$product_meta           = $this->db->_fetch_product_by_id( $data['item'] );				
			$item_meta['value']     = ( 
				floatval( 
					str_replace( ',', '.', 
						$product_meta['price'] 
					) 
				) * (int)$data['item_amount'] 
			);
			$item_meta['timex']     = date("Y-m-d H:i:s");	
			$item_meta['year']      = date("Y");
			$item_meta['month']     = date("m");
			$item_meta['day']       = date("d");
			$item_meta['client'] 	= $data['client'];
			$item_meta['obs']    	= $data['obs'];	
			$item_meta['owner']  	= $_SESSION['user_id'];	





			if( empty($data['item']) || empty( $data['item_amount'] ) ){
				die( json_encode( array( 'error' => 'You can\'t sell more than you already have!', 'itemCount' => $product_meta['amount'], 'type' => 'blank_fields' ) ) );

			}
			if( $product_meta['amount'] < $data['item_amount'] ){
				die( json_encode( array( 'error' => 'You can\'t sell more than you already have!', 'itemCount' => $product_meta['amount'] ) ) );

			}
			$this->db->_update_item( $data['item'], ( $product_meta['amount'] - $data['item_amount'] )  );
			// exit( print_r($item_meta));
			if( $this->db->_insert( 'sells', $item_meta ) ){
				die( json_encode( array( 'success' => 'Product saved!' ) ) );
			}

		}
		public function get_products(){
			// $sqlSelect = "SELECT * from `products`";
			$data = $this->db->_fetch_data();
			return $data;
		}
		public function get_sells(){
			// $sqlSelect = "SELECT * from `products`";
			$data = $this->db->_fetch_sells_only();
			return $data;
		}
		public function get_sells_value(){
			return array_reverse($this->db->_fetch_value_of_sells());
		}
		public function update_single($key, $data){
			// $sqlSelect = "SELECT * from `products`";
			$data = $this->db->_update_item_prop($key, $data);
			return $data;
		}
		public function get_product_by_id($id){
			return $this->db->_fetch_product_by_id($id);
		}
		public function get_sells_date($month, $year){
			return $this->db->_fetch_sell_between_date($month, $year);
		}
	}
