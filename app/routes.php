<?php
  $app = new Main_Bootstrap();
  $app->init();
	$main_ctrl = new Main_Controller();

	if( isset( $_GET['sair'] ) && $main_ctrl->is_logged_in() ) {
		$main_ctrl->log_out();		
	}

	if( isset( $_POST['password'] ) && isset( $_POST['uname'] ) && $main_ctrl->is_logged_in() ) :
		$main_ctrl->check_user( $_POST['uname'], $_POST['password'] );
	exit;	
	endif;

  if( isset( $_POST['internal_action'] ) && $_POST['internal_action'] == "add_item"  && $main_ctrl->is_logged_in()) :    
    $main_ctrl->add_item( $_POST );
  exit; 
  endif;

  if( isset( $_POST['internal_action'] ) && $_POST['internal_action'] == "sell_item"  && $main_ctrl->is_logged_in()) :    
    $main_ctrl->sell_item( $_POST );
  exit; 
  endif;
  if( isset( $_POST['internal_action'] ) && $_POST['internal_action'] == "edit_item"  && $main_ctrl->is_logged_in()) :    
    $main_model = new Main_Model($_POST['item_id']);

    $new_meta = array(            
      'id'          => $_POST['item_id'],
      'title'       => $_POST['title'],
      'code'        => $_POST['code'],
      'description' => $_POST['description'],
      'price'       => $_POST['value'],
      'amount'      => $_POST['amount'],
      'owner'       => $_POST['owner'],
      'year'        => $_POST['item_year'],
      'month'       => $_POST['item_month'],
      'day'         => $_POST['item_day'],
    );

    foreach ($new_meta as $key => $value) {
      $main_ctrl->update_item_single( $key, $new_meta );
    }
    die( json_encode(array( 'success' => 'Product edited!' )) );
    
  exit; 
  endif;
  if( isset( $_GET['edit'] ) && $_GET['id'] != null  && $main_ctrl->is_logged_in()){
    $main_model = new Main_Model($_GET['id']);
  }
  var_dump($_GET);
  if( isset( $_GET['delete'] ) && $_GET['delete'] == "item"  && $_GET['id'] != null){
    echo '<h1>eut user';
    die();
  }