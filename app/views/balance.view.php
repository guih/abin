			  <h2 class="sub-header"><i class="fa fa-money"></i> Balanço </h2>
				<form  action="index.php?balance" method="get">
				  <input type="hidden" value="" name="balance">
				  <p>Intervalo</p>
				  Mês
				  <select class="" name="month">
					<?php 
						$data = $main_ctrl->fetch_sells();
						
						$is_current_month = '';  
						$data_month = array();
						foreach ($data as $key => $value) {
							 // var_dump(in_array( $value['month'], $data_month ));
							if( ! in_array( $value['month'], $data_month ) ){
								if( isset( $_GET['month'] ) ){
									if( $_GET['month'] !== $value['month'] ){
										$is_current_month = '';
									}else{
										$is_current_month = 'selected';
									}
								}                        
								echo '<option '.$is_current_month.' value="'.$value['month'].'">'.$main_ctrl->get_month_ame_by_number($value['month']).'</option>';                        
								$data_month[] = $value['month'];
							}

						}
					?>                
				  </select>
				  Ano                  
				  <select class="" name="year">
					<?php 
						$is_current_year = '';
						$data_year = array();
						foreach ($data as $key => $value) {

							if( ! in_array( $value['year'], $data_year ) ){
								if( isset( $_GET['year'] ) ){
									if( $_GET['year'] !== $value['year'] ){
										$is_current_year = '';
									}else{
										$is_current_year = 'selected';
									}
								}
								echo '<option '.$is_current_year.' value="'.$value['year'].'">'.$value['year'].'</option>';                        
							}
							$data_year[] = $value['year'];
						}
					?>    
				  </select>
				  <button type="submit" class="btn-primary btn"> <i class="fa fa-filter"></i> filtrar</button>
				</form>
				  <div class="table-responsive">
					<table class="table table-products table-striped table-hover">
					  <thead>
						<tr>
						  <th>Vendidos</th>
						  <th>Título</th>
						  <th>Acrécimos</th>
						  <th>Total</th>
						  <th>Observações</th>
						  <th>Cliente</th>
						  <th>Data</th>
						</tr>
					  </thead>
					  <tbody>
						
						  <?php 
						  if( isset( $_GET['month'] ) && isset( $_GET['year'] ) ){
							$data = $main_ctrl->get_sell_in_date( $_GET['month'], $_GET['year'] );
						  }
						  $sorted_data = $data;
						  foreach ($sorted_data as $key => $value) {
							$item_meta = new Main_Model($data[$key]['parent_id']);
							$client_meta = new Client_Model($data[$key]['client']);

							$acre = ( $value['acre'] == 0 ) ? '<strong>N/A</strong>' : $value['acre'] . ' R$';
							$obs = ( $value['obs'] == "" ) ? '<strong>N/A</strong>' : $value['obs'];

							$ifcli = $client_meta->name;
							// var_dump($ifcli);
							$cli = ( $ifcli == null ) ? '<strong>N/A</strong>' : $ifcli;

							
							$markup = "";
							$markup .= "<tr>";
							$markup .= "<td>{$value['amount']} x</td>";
							$markup .= "<td>{$item_meta->title}</td>";
							$markup .= "<td>{$acre}</td>";
							$markup .= "<td>{$value['value']} R$</td>";
							$markup .= "<td>{$obs}</td>";
							$markup .= "<td>{$cli}</td>";
							$markup .= "<td>".date($value['timex'])."</td>";
							$markup .= "</tr>";

							print $markup;       
						  }
						  ?>

					  </tbody>
					</table>
              		<span class="inf"><?php echo count( $data ); ?> vendas<?php  echo ( isset( $_GET['month'] ) && isset( $_GET['year'] ) ) ? ' neste período.': '.'; ?></span>
				  </div>