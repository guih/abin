          <?php $_SESSION['title'] = "d"; ?>

          <h2 class="sub-header">Editar produto<a href="?stock" class="btn btn-primary pull-right">Editar outro item</a></h2>

          <form role="form" id="edit_item">
            <div class="form-group">
              <label for="exampleInputEmail1">Codigo</label>
              <input name="code" type="text" class="form-control" value="<?php echo $main_model->code; ?>" placeholder="Enter email">
            </div>  
            <div class="form-group">
              <label for="exampleInputEmail1">Titulo</label>
              <input name="title" type="text" class="form-control" value="<?php echo $main_model->title; ?>" placeholder="Enter email">
            </div>  
            <div class="form-group">
              <label for="exampleInputEmail1">Descrição</label>
              <textarea class="form-control" name="description"><?php echo $main_model->description; ?></textarea>
            </div>  
            <div class="form-group">
              <label for="exampleInputEmail1">Valor</label>
              <input name="value" type="text" class="form-control" value="<?php echo $main_model->price; ?>" placeholder="Enter email">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Quantidade</label>
              <input name="amount" type="text" class="form-control" value="<?php echo $main_model->amount; ?>" placeholder="Enter email">
              <input type="hidden" name="internal_action" value="edit_item">

              <input type="hidden" name="item_id" value="<?php echo $main_model->id; ?>">

              <input type="hidden" name="item_year" value="<?php echo $main_model->year; ?>">
              <input type="hidden" name="item_month" value="<?php echo $main_model->month; ?>">
              <input type="hidden" name="item_day" value="<?php echo $main_model->day; ?>">
              <input type="hidden" name="owner" value="<?php echo $_SESSION['user_id']; ?>">



              
            </div>  

            <a href="?stock" class="btn btn-default">Cancelar</a>
            <a href="javascript:;" class="btn btn-primary save-edit-item">Salvar</a>

          </form>