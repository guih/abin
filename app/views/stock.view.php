            <h2 class="sub-header"><i class="fa fa-th-large"></i> Estoque <button type="button" class="btn open-add-product btn-primary pull-right"> <i class="fa fa-plus"></i> Adicionar produto</button></h2>
            <div class="table-responsive">
              <table class="table table-products table-striped table-hover">
                <thead>
                  <tr>
                    <th>Cod.</th>
                    <th>Título</th>
                    <th>Description</th>
                    <th>Quantidade</th>
                    <th>Preço</th>
                    <th>Opções</th>

                  </tr>
                </thead>
                <tbody>
                  
                    <?php 
                    $data = $main_ctrl->fetch_items(); 
                    foreach ($data as $key => $value) {
                      if( isset( $value['title'] ) && $value['title'] != "" ){
                        $is_in_stock = ( $value['amount'] == "-1") ? '<strong>Fora de estoque</strong>' : $value['amount'];
                        $markup = "<tr>";
                        $markup .= "<td>{$value['code']}</td>";
                        $markup .= "<td>{$value['title']}</td>";
                        $markup .= "<td>{$value['description']}</td>";
                        $markup .= "<td>{$is_in_stock}</td>";
                        $markup .= "<td>{$value['price']}</td>";
                        $markup .= "<td><a href=\"?edit=item&id={$value['id']}\">Editar</a> <a href=\"?delete=item&id={$value['id']}\">Excluir</a> </td>";

                        $markup .= "</tr>";

                        print $markup;      
                        
                      }

                    }
                    ?>

                </tbody>
              </table>              
              <span class="inf"><?php echo count( $data ); ?> produtos cadastrados.</span>

            </div>
          </div>
        </div>
      <div class="modal fade" id="add-product">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
              <h4 class="modal-title">Adicionar Produto</h4>
            </div>
            <div class="modal-body">
            <form role="form" id="product-addition-form">
                  <input type="hidden" name="internal_action" value="add_item">

                  <div class="form-group">
                    <label for="recipient-name" class="control-label">Código</label>
                    <input type="text" name="item_code" placeholder="Ex. 0054dffd45f4ddsfd45f4. Caso vazio será gerado automaticamente." class="form-control" id="code">
                  </div>
                  <div class="form-group">
                    <label for="message-text" class="control-label">Título</label>
                    <input type="text" placeholder="Camiseta Polo Masc. P/M/G" class="form-control" id="title" name="item_title">                 
                  </div>
                  <div class="form-group">
                    <label for="message-text" class="control-label">Descrição:</label>
                    <textarea class="form-control" id="description" name="item_description"></textarea>
                  </div>
<!--                   <div class="form-group">
                    <label for="recipient-name" class="control-label">Preço</label>
                    <input type="text" placeholder="599,85 R$" class="form-control" id="price" name="item_price">
                  </div> -->
                  <div class="input-group">
                    <input type="text" placeholder="599,85" class="form-control" id="price" name="item_price">                  
                    <span class="input-group-addon">R$ </span>
                  </div>
                  <div class="form-group">
                    <label for="recipient-name" class="control-label">Quantidade</label>
                    <input type="number" placeholder="999" class="form-control" id="amount" name="item_amount">
                  </div>
                </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary add-product-item-submit">Salvar</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      </div>