    <div class="container main_login">
      <form class="form-signin" role="form">
      <h2 class="form-signin-heading">Login</h2>
      <label for="inputEmail" class="sr-only">Usuário</label>
      <input type="text" id="inputEmail" class="form-control" placeholder="Usuário" name="uname" required autofocus>
      <label for="inputPassword" class="sr-only">Senha</label>
      <input type="password" id="inputPassword" class="form-control" placeholder="Combinação" name="password" required>
      <div class="message">Ops!</div>
      <div class="checkbox">
        <label>
        <input type="checkbox" value="remember-me"> Manter concetado
        </label>
      </div>
      <button class="btn try-login btn-lg btn-primary btn-block" type="submit">Entrar</button>
          <p> Powered by <a href="mailto:guih@hotmail.com.br">Guilherme Henrique</a> </p>
      </form><!-- /Login form -->

    </div> <!-- /container -->