<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">

	<title><?php echo $main_ctrl->get_title_by_scrren(); ?> - Controle de estoque</title>

	<!-- Bootstrap core CSS -->
	<link href="app/public/css/bootstrap.min.css" rel="stylesheet">
	<link href="app/public/css/style.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript">
	  <?php echo 'window.dataT = ' . json_encode( $main_ctrl->get_value_of_sells() );  ?>

	</script>
  </head>

  <body>

	<?php  

	if( ! $main_ctrl->is_logged_in() ){
		include  "app/views/login.view.php"; 
	}else{
		include  "app/views/top.left.view.php";

		if( isset( $_GET['edit'] ) && $_GET['edit'] == "item" 
			&& $_GET['id'] != null ){
			include 'app/views/edit.item.view.php';
		}else{
			if( isset( $_GET['edit'] ) && $_GET['edit'] == "user" 
				&& $_GET['id'] != null ){
				include 'app/views/edit.user.view.php';
			}else{
					if( isset( $_GET['edit'] ) && $_GET['edit'] == "client" 
						&& $_GET['id'] != null ){
							include 'app/views/edit.client.view.php';
						
					}else{
						if( isset( $_GET['stock'] ) ){
							include 'app/views/stock.view.php';
						}else{
							if( isset( $_GET['balance'] ) ){
								include 'app/views/balance.view.php';
							}else{
								if( isset( $_GET['sell'] ) ){
									include 'app/views/sell.view.php';
								}else{
									if( isset( $_GET['profile'] ) ){
										include 'app/views/profile.view.php';
									}else{
										if( isset( $_GET['clients'] ) ){
											include 'app/views/clients.view.php';
										}else{
											if( isset( $_GET['users'] ) ){
												include 'app/views/users.view.php';

											}else{
												?>
												<h2 class="sub-header"> <i class="fa fa-dashboard"></i> Home</h2>
												<canvas id="myChart" width="100%" height="400"></canvas>
												<?php 
											} 
										}
									}
								}
							}
						}
					}
				}
			}
		}
	  ?>
			
				</div>
			</div>
		</div>
	<script src="app/public/js/Zepto.js"></script>
	<script src="app/public/js/chart.js"></script>
	<script src="app/public/js/choosen.js"></script>
	<script src="app/public/js/App.js"></script>
	<script src="app/public/js/bootstrap.js"></script>
  </body>
</html>
