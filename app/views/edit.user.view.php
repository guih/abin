		<?php
			$user_meta = $main_ctrl->fetch_single_user($_GET['id']);
	   	?>

		<h2 class="sub-header">Editar usuário<a href="?users" class="btn btn-primary pull-right">Editar outro usuário</a></h2>

		<form role="form" id="edit_user">
			<div class="form-group">
				<label for="exampleInputEmail1">Nome</label>
				<input name="name" type="text" class="form-control" value="<?php echo $user_meta['name']; ?>" placeholder="Nome">
			</div>  
			<div class="form-group">
				<label for="exampleInputEmail1">Email</label>
				<input name="email" type="text" class="form-control" value="<?php echo $user_meta['email']; ?>" placeholder="Email">
			</div>  
			<div class="form-group">
				<label for="exampleInputEmail1">Senha</label>
				<input name="password" type="text" class="form-control" value="" placeholder="Deixe em branco para não alterar">
			</div>  
			<div class="form-group">
				<input type="hidden" name="user_id" value="<?php echo $_GET['id']; ?>">       
				<input type="hidden" name="internal_action" value="edit_user">
			</div>
			
			<a href="?users" class="btn btn-default">Cancelar</a>
			<a href="javascript:;" class="btn btn-primary save-edit-user">Salvar</a>

		</form>