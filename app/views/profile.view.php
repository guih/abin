					<?php
				  		$user = new User_Model( $_SESSION['user_id'] );
				   	?>
						<div class="col-xs-4 col-sm-2">
							<form method="POST" id="change_avatar_user" enctype="multipart/form-data" style="display:none">
								<input type="hidden" name="internal_action" value="change_avatar">
								<input type="file" name="avatar" value="">
								<input type="submit">
							</form>
							<img class="img-thumbnail" src="<?php  echo $user->avatar; ?>">
							<h4><?php echo $user->name; ?></h4>
							<p class="text-muted"><?php echo $user->level; ?></p>
							<div class="changed_avatar col-xs-1">
								<a class="btn btn-primary select-pic"><i class="fa fa-upload"></i> Escolher foto...</a>
								<a class="btn btn-default send-pic"> Enviar</a>	
							</div>	
						</div>
						<div class="changed_avatar col-xs-10">

							<div class="col-xs-10">
		   					<h2 class="sub-header"><i class="fa fa-money"></i>  Vendas deste usuário</h2>

							<div class="table-responsive">
							  <table class="table table-clients table-striped table-hover">
								<thead>
								  <tr>
									  <th>Vendidos</th>
									  <th>Título</th>
									  <th>Acrécimos</th>
									  <th>Total</th>
									  <th>Observações</th>
									  <th>Cliente</th>
									  <th>Data</th>
								  </tr>
								</thead>
								<tbody>
								  
									<?php 
									$data = $user->sells; 
							
									foreach ($data as $key => $value) {
									
										$item_meta = new Main_Model($data[$key]['parent_id']);
										$client_meta = new Client_Model($data[$key]['client']);
										
										$markup = "";
										$markup .= "<tr>";
										$markup .= "<td>{$value['amount']} x</td>";
										$markup .= "<td>{$item_meta->title}</td>";
										$markup .= "<td>{$value['acre']}</td>";
										$markup .= "<td>{$value['value']} R$</td>";
										$markup .= "<td>{$value['obs']}</td>";
										$markup .= "<td>{$client_meta->name}</td>";
										$markup .= "<td>".date('Y-d-m', strtotime( $value['timex']))."</td>";
										$markup .= "</tr>";

										print $markup;       
									}
									?>

								</tbody>
							  </table>
							</div>
							</div>				
						</div>