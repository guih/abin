     <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="?">Controle de estoque</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="?"><i class="fa fa-home"></i> Home</a></li>
            <li <?php echo ( isset( $_GET['profile'] ) ) ? ' class="active" ' : ''; ?>><a href="?profile"><i class="fa fa-user"></i> Perfil</a></li>
            <li><a href="#"><i class="fa fa-upload"></i> Exportar</a></li>
            <li <?php echo ( isset( $_GET['users'] ) ) ? ' class="active" ' : ''; ?>><a href="?users"><i class="fa fa-users"></i> Usuários</a></li>
            <!-- <li><a href="?help">Ajuda</a></li> -->
            <li><a href="?sair"><i class="fa fa-sign-out"></i>Sair</a></li>
          </ul>
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control find-product" placeholder="Buscar...">
          </form>
        </div>
      </div>
    </nav>
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-3 col-md-2 sidebar" style="<?php echo ( count($_GET) == 0 ? 'display:none' : '' ); ?>">
            <ul class="nav nav-sidebar">
              <li <?php if( ! isset( $_GET['stock'] ) && !isset( $_GET['sell'] ) && !isset( $_GET['balance'] ) && !isset( $_GET['clients'] )&& !isset( $_GET['users'] ) && !isset( $_GET['profile'] ) ){ ?>class="active" <?php }?>><a href="?"> <i class="fa fa-dashboard"></i> Geral </a></li>
              <li <?php if(  isset( $_GET['stock'] ) ){ ?>class="active" <?php }?>><a href="?stock"><i class="fa fa-th-large"></i> Estoque</a></li>
              <li <?php if( isset( $_GET['balance'] ) ){?> class="active"<?php } ?>><a href="?balance"><i class="fa fa-money"></i> Balanço</a></li>
              <li <?php if( isset( $_GET['sell'] ) ){?> class="active"<?php } ?>><a href="?sell"><i class="fa fa-exchange"></i> Vender</a></li>
              <li <?php if( isset( $_GET['clients'] ) ){?> class="active"<?php } ?>><a href="?clients"><i class="fa fa-users"></i> Clientes</a></li>

            </ul>
            <p> Powered by <a href="mailto:guih@hotmail.com.br">Guilherme Henrique</a> </p>

          </div>
          <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
