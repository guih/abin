				<h2 class="sub-header">Usuários <button type="button" class="btn open-add-user btn-primary pull-right"><i class="fa fa-plus"></i> Adicionar usuário</button></h2>
				<div class="table-responsive">
				  <table class="table table-products table-striped table-hover">
					<thead>
					  <tr>
						<th>Cod.</th>
						<th>Nome</th>
						<th>Email</th>
						<th>Nível</th>
						<th>Opções</th>
					  </tr>
					</thead>
					<tbody>
					  
						<?php 
						$data = $main_ctrl->fetch_users(); 
						foreach ($data as $key => $value) {	

						  $markup = "<tr>";
						  $markup .= "<td>{$value['id']}</td>";
						  $markup .= "<td>{$value['name']}</td>";
						  $markup .= "<td>{$value['email']}</td>";
						  $markup .= "<td>{$value['level']}</td>";
						  $markup .= "<td><a href=\"?edit=user&id={$value['id']}\">Editar</a> <a href=\"?delete=user&id={$value['id']}\">Excluir</a> </td>";

						  $markup .= "</tr>";

						  print $markup;       
						}

						?>

					</tbody>
				  </table>

				  <span class="inf"><?php echo count( $data ); ?> usuários cadastrados.</span>
				</div>
			  </div>
			</div>
		  <div class="modal fade" id="add-user">
			<div class="modal-dialog">
			  <div class="modal-content">
				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				  <h4 class="modal-title">Adicionar Usuário</h4>
				</div>
				<div class="modal-body">
				<form role="form" id="user-addition-form">
					  <input type="hidden" name="internal_action" value="add_item">

					  <div class="form-group">
						<label for="recipient-name" class="control-label">Nome</label>
						<input type="text" name="name" placeholder="" class="form-control" id="code">
					  </div>
					  <div class="form-group">
						<label for="message-text" class="control-label">Email</label>
						<input type="email" placeholder="nome@exemplo.com" class="form-control" id="title" name="email">                 
					  </div>
					  <div class="form-group">
						<label for="message-text" class="control-label">Senha</label>
						<input type="password" placeholder="" class="form-control" id="title" name="password">       
						<input type="hidden" placeholder="" class="form-control" value="add_user" name="internal_action">                 

						
					  </div>
					  <div class="form-group">
						<label for="recipient-name" class="control-label">Nível</label>
						<select name="user_level" class="form-control">
						  <option value="god">Admin</option>
						  <option value="human">Usuário</option> 
						</select>
					  </div>
					</form>
				</div>
				<div class="modal-footer">
				  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				  <button type="button" class="btn btn-primary add-user-submit">Salvar</button>
				</div>
			  </div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		  </div><!-- /.modal -->
		  </div>