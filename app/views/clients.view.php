							<h2 class="sub-header">Clientes <button type="button" class="btn open-add-client btn-primary pull-right"><i class="fa fa-plus"></i> Adicionar cliente</button></h2>
							<div class="table-responsive">
							  <table class="table table-clients table-striped table-hover">
								<thead>
								  <tr>
									<th>#</th>
									<th>RG</th>
									<th>Expira em</th>
									<th>Ativo</th>
									<th>Nome</th>
									<th>Email</th>
									<th>CPF</th>
									<th>Opções</th>
								  </tr>
								</thead>
								<tbody>
								  
									<?php 
									$data = $main_ctrl->get_all_clients(); 
									foreach ($data as $key => $value) {
									
									  $markup = "<tr>";
									  $markup .= "<td style=\"height:15px \">{$value['id']}</td>";
									  $markup .= "<td style=\"height:15px\">{$value['rg']}</td>";
									  $markup .= "<td style=\"height:15px\">{$value['expire']}</td>";
									  $markup .= "<td style=\"height:15px\">{$value['active']}</td>";
									  $markup .= "<td style=\"height:15px\">{$value['name']}</td>";
									  $markup .= "<td style=\"height:15px\">{$value['email']}</td>";
									  $markup .= "<td style=\"height:15px\">{$value['cpf']}</td>";

									  $markup .= "<td><a href=\"?edit=client&id={$value['id']}\">Editar</a> <a href=\"?delete=client&id={$value['id']}\">Excluir</a> </td>";

									  $markup .= "</tr>";

									  print $markup;       
									}
									?>

								</tbody>
							  </table>
              				  <span class="inf"><?php echo count( $data ); ?> clientes cadastrados.</span>

							</div>
							<div class="modal fade" id="add-client">
								<div class="modal-dialog">
								  <div class="modal-content">
									<div class="modal-header">
									  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
									  <h4 class="modal-title">Adicionar Cliente</h4>
									</div>
									<div class="modal-body">
									<form role="form" id="client-addition-form">

										  <div class="form-group">
											<label for="recipient-name" class="control-label">Nome</label>
											<input type="text" name="client[name]" placeholder="" class="form-control" id="code">
										  </div>
										  <div class="form-group">
											<label for="recipient-name" class="control-label">CPF</label>
											<input type="text" name="client[cpf]" placeholder="123.456.789-20" class="form-control">
										  </div>
										  <div class="form-group">
											<label for="recipient-name" class="control-label">Ativo</label>
											<select name="client[active]" placeholder="" class="form-control">
												<option value="1">Sim</option>
												<option value="0">Não</option>

											</select>
										  </div>
										  <div class="form-group">
											<label for="message-text" class="control-label">RG</label>
											<input type="text" placeholder="" class="form-control" id="title" name="client[rg]">                 
										  </div>
										   <div class="form-group">
											<label for="message-text" class="control-label">Email</label>
											<input type="email" placeholder="nome@exemplo.com" class="form-control" id="title" name="client[email]">                 
										  </div>
										  <div class="form-group">
											<label for="message-text" class="control-label">Expira em</label>
											<input type="text" id="expires" placeholder="" class="form-control" id="title" name="client[expire]">       
											<input type="hidden" placeholder="" class="form-control" value="add_client" name="internal_action">                 

											
										  </div>
											<!--  -->
									</form>
									</div>
									<div class="modal-footer">
									  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
									  <button type="button" class="btn btn-primary add-client-submit">Salvar</button>
									</div>
								  </div><!-- /.modal-content -->
								</div><!-- /.modal-dialog -->
							</div><!-- /.modal -->