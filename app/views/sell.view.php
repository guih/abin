
      <h2 class="sub-header"><i class="fa fa-exchange"></i> Vender </h2>
        <form  role="form" id="sell_form">
          <div class="form-group f_40">
            <select id="pick-item" name="item">
              <option selected>Selecione um item</option>
              <?php  
              foreach ($main_ctrl->fetch_items() as $key => $value)
                echo "<option value=\"{$value['id']}\">{$value['title']} - {$value['price']}</option>";
              ?>
            </select>
          </div>
          <div class="form-group">
            <input type="text" class="form-control f_20 f_l" name="item_amount" placeholder="Quantidade">
            <input type="text" class="form-control f_20" name="item_acre" placeholder="Acrécimos">
            <input type="hidden" name="internal_action" value="sell_item">
            <div class="message"></div>

          </div>
          <p> <a href="javascript:;" class="to_user">Vincular cliente a esta venda</a> <a href="javascript:;" class="to_user to_user_c">Cancelar</a> </p>
         <div class="form-group f_40 v-client">
          <select id="pick-client" name="client">
              <option selected>Selecione um cliente</option>
              <?php  
              foreach ($main_ctrl->get_all_clients() as $key => $value)
                echo "<option value=\"{$value['id']}\">{$value['name']}</option>";
              ?>
          </select>
        </div>
        <div class="form-group f_40 v-client">
          <textarea name="obs" class="form-control" placeholder="Observações"></textarea>
        </div>
          <button type="submit" class="btn btn-success go-sell-item">Vender</button>
        </form>