		<?php
			$client_meta = $main_ctrl->fetch_single_client($_GET['id']);
	   	?>

		<h2 class="sub-header">Editar cliente<a href="?clients" class="btn btn-primary pull-right">Editar outro cliente</a></h2>

		<form role="form" id="edit_user">
			<div class="form-group">
				<label for="exampleInputEmail1">Nome</label>
				<input name="name" type="text" class="form-control" value="<?php echo $client_meta['name']; ?>" placeholder="Nome">
			</div>  
			<div class="form-group">
				<label for="exampleInputEmail1">RG</label>
				<input name="rg" type="text" class="form-control" value="<?php echo $client_meta['rg']; ?>" placeholder="Nome">
			</div>  
			<div class="form-group">
				<label for="exampleInputEmail1">Expira em</label>
				<input name="expire" type="text" class="form-control" value="<?php echo $client_meta['expire']; ?>" placeholder="Email">
			</div>  
			<div class="form-group">
				<label for="exampleInputEmail1">Email</label>
				<input name="email" type="email" class="form-control" value="<?php echo $client_meta['email']; ?>" placeholder="Email">
			</div>  
			<div class="form-group">
				<label for="exampleInputEmail1">CPF</label>
				<input name="cpf" type="text" class="form-control" value="<?php echo $client_meta['cpf']; ?>" placeholder="Email">
			</div>  
			<div class="form-group">
				<label for="exampleInputEmail1">Ativo</label>
				<select name="active">
					<option value="1">Sim</option>
					<option value="0">Não</option>
				</select>
			</div>  
			<div class="form-group">
				<input type="hidden" name="client_id" value="<?php echo $_GET['id']; ?>">       
				<input type="hidden" name="internal_action" value="edit_client">
			</div>
			
			<a href="?users" class="btn btn-default">Cancelar</a>
			<a href="javascript:;" class="btn btn-primary save-edit-user">Salvar</a>

		</form>