<?php  
	session_start();
	class Main_Bootstrap {
		public function __construct()
		{
			error_reporting(-1);
			ini_set('display_errors', 'On');
			ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
		}
		public function init()
		{
			$this->__register_autoload();
			// $this->site_frontend();
		}

		public function __register_autoload()
		{		
			spl_autoload_register( array( $this, '_autoload' ) );
			call_user_func( array( $this, 'site_frontend' ) );
		}

		public function _autoload( $class_name )
		{
			$c_name = strtolower( $class_name . ".php" );

			$class_id = explode( '.', $c_name );

			$type = explode( '_', $class_id[0] );

			switch( $type[1] ) :
				case "model":
					$include_dir = "models";
					break;	
				case "controller":
					$include_dir = "controllers";
					break;
				default:
					$include_dir = null;		
					break;	
			endswitch;	
			if( $include_dir != null )
				require( $include_dir . '/' . str_replace( '_', '.', $c_name ) );
		}

		public function site_frontend(){
			$main_ctrl = new Main_Controller();

			if( isset( $_GET['sair'] ) && $main_ctrl->is_logged_in() ) {
				$main_ctrl->log_out();		
			}

			if( isset( $_POST['password'] ) && isset( $_POST['uname'] ) ) :
				$main_ctrl->check_user( $_POST['uname'], $_POST['password'] );
			exit;	
			endif;

		  if( isset( $_POST['internal_action'] ) && $_POST['internal_action'] == "change_avatar" && $main_ctrl->is_logged_in() ) :
			// print_r($_FILES);
			$files = $_FILES;
			// $fname $_FILES['name'];
			if( !isset( $files['avatar'] ) || $files['avatar']['name'] == "" )
			  exit('Escolha uma foto para continuar. <a href="javascript:history.back(-1)">Voltar a  pagina anterior</a>');

			$type = $files['avatar']['type'];
			$type_single = explode( '/', $type );
			$fname_single = explode( '.'.$type_single[1], $files['avatar']['name'] );

			$allowed = array( 
			  'image/png',
			  'image/jpeg',
			  'image/jpg',
			);

			$fname = str_replace( ' ', '_', $fname_single[0] ) . '.' . $type_single[1];
			if( in_array($type, $allowed) ){
			 	
			   // print_r();
				// print_r($fname);
			   if( move_uploaded_file($files['avatar']['tmp_name'], 'tmp/user/'.$fname) ){
				$data_avatar = array( 
				  'id'     => $_SESSION['user_id'], 
				  'avatar' => 'tmp/user/'.$fname 
				);
				foreach( $data_avatar as $key => $data ){
				  $main_ctrl->update_user_avatar( $key, $data_avatar );          
				}
				print '<script>window.location.href="?profile";</script>';
			   }else{
				  exit('Not acceptable! <a href="javascript:history.back(-1)">Voltar a  pagina anterior</a>');
			   }

			}else{
			 exit('Not acceptable! <a href="javascript:history.back(-1)">Voltar a  pagina anterior</a>');
			 // if( move_uploaded_file(filename, destination) )
			}

		  exit; 
		  endif;


		  if( isset( $_POST['internal_action'] ) && $_POST['internal_action'] == "add_item"  && $main_ctrl->is_logged_in() ) :    
			$main_ctrl->add_item( $_POST );
		  exit; 
		  endif;

		  if( isset( $_POST['internal_action'] ) && $_POST['internal_action'] == "add_user"  && $main_ctrl->is_logged_in() ) :    
			$main_ctrl->add_user_to_system( 'users', array(
			  'name'     => $_POST['name'],
			  'email'    => $_POST['email'],
			  'password' => @md5( $_POST['password'] ),
			  'level'    => $_POST['user_level'],
			) );
		  exit; 
		  endif;
		  
		  if( isset( $_POST['internal_action'] ) && $_POST['internal_action'] == "add_client"  && $main_ctrl->is_logged_in() ) :    
			$main_ctrl->add_client_to_system('clients', $_POST['client'] );
		  exit; 
		  endif;
			
		  if( isset( $_POST['internal_action'] ) && $_POST['internal_action'] == "sell_item"  && $main_ctrl->is_logged_in() ) :    
			$main_ctrl->sell_item( $_POST );
		  exit; 
		  endif;

		  if( isset( $_POST['internal_action'] ) && $_POST['internal_action'] == "edit_user"  && $main_ctrl->is_logged_in() ) :    
			$new_user_meta = array(            
			  'id'       => $_POST['user_id'],
			  'name'     => $_POST['name'],
			  'email'    => $_POST['email'],
			  'password' => $_POST['password'],
			);
			foreach( $new_user_meta as $key => $value) {
			  $main_ctrl->update_user( $key, $new_user_meta );
			}
			
			die( json_encode(array( 'success' => 'User edited!' )) );

		  exit; 
		  endif;

		  if( isset( $_POST['internal_action'] ) && $_POST['internal_action'] == "edit_item"  && $main_ctrl->is_logged_in() ) :    
			$main_model = new Main_Model($_POST['item_id']);

			$new_meta = array(            
			  'id'          => $_POST['item_id'],
			  'title'       => $_POST['title'],
			  'code'        => $_POST['code'],
			  'description' => $_POST['description'],
			  'price'       => $_POST['value'],
			  'amount'      => $_POST['amount'],
			  'owner'       => $_POST['owner'],
			  'year'        => $_POST['item_year'],
			  'month'       => $_POST['item_month'],
			  'day'         => $_POST['item_day'],
			);

			foreach ($new_meta as $key => $value) {
			  $main_ctrl->update_item_single( $key, $new_meta );
			}
			die( json_encode(array( 'success' => 'Product edited!' )) );
			
		  exit; 
		  endif;
		  if( isset( $_GET['edit'] ) && $_GET['id'] != null  && $main_ctrl->is_logged_in()){
			$main_model = new Main_Model($_GET['id']);
		  }

		  include 'app/views/home.view.php';
		}

	}

?>