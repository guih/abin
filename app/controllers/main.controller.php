<?php 
	include "app/helpers/users.helper.php";
	include "app/helpers/products.helper.php";
	include "app/helpers/clients.helper.php";

	class Main_Controller {
		private $user_helper;
		private $products_helper;
		private $clients_helper;

		public function __construct(){
			$this->user_helper     = new Users_Helper();
			$this->products_helper = new Products_Helper( $this->user_helper->get_pdo_handler() );
			$this->clients_helper  = new Clients_Helper();			
		}

		public function check_user($user, $pass){
			$user_meta = $this->user_helper->match_user( $user );
			if( $user_meta === false ){
				die( json_encode( array('error' => 'No user with this nickname', 'type' => 'not_found_nickname' ) ) );
			}else{
				$this->user_helper->log_user( $user_meta, array($user, $pass) );
			}
	
		}
		public function add_item($meta){
			$this->products_helper->insert_product($meta);
		}
		public function fetch_single_user($id){
			return $this->user_helper->single_user($id);
		}
		public function update_user_avatar($key, $data){
			$this->user_helper->update_user_single($key, $data);
		}
		public function sell_item($data){
			$this->products_helper->sell( $data );
		}
		public function get_user_sells($id){
			return $this->user_helper->fetch_user_sells( $id );
		}
		public function fetch_items(){
			return $this->products_helper->get_products();
		}
		public function fetch_sells(){
			return $this->products_helper->get_sells();
		}
		public function fetch_single_client( $id ){
			return $this->clients_helper->get_client_by_id( $id );
		}
		public function fetch_item_by_id($id){
			return $this->products_helper->get_product_by_id($id);
		}
		public function is_logged_in(){
			if( isset($_SESSION) )
				return isset( $_SESSION['user_token'] );
		}
		public function get_value_of_sells(){
			return $this->products_helper->get_sells_value();
		}
		public function update_item_single($key, $data){
			$this->products_helper->update_single( $key, $data );
		}
		public function update_user($key, $data){
			$this->user_helper->update_user_single( $key, $data );
		}
		public function add_client_to_system($where='clients', $meta){
			return $this->clients_helper->add_client( $where, $meta );
		}
		public function get_all_clients(){
			return $this->clients_helper->get_clients();
		}
		public function log_out(){
			$this->user_helper->logg_out();
		}
		public function get_sell_in_date( $month, $year ){
			return $this->products_helper->get_sells_date($month, $year);
		}
		public function fetch_users(){
			return $this->user_helper->get_users();
		}
		public function add_user_to_system($where='users', $meta){
			return $this->user_helper->add_user( $where, $meta );
		}
		public function get_month_ame_by_number($month){
			$months = array(
				'Janeiro',
				'Fevereiro', 
				'Março', 
				'Abril', 
				'Maio',
				'Junho', 
				'Julho', 
				'Agosto', 
				'Setembro', 
				'Outubro', 
				'Novembro', 
				'Dezembro'
			);
			return $months[$month-1];
		}

		public function get_title_by_scrren(){
			$page  = $_GET;
			$title = "";
			switch ($_GET) {
				case !isset($_GET):
					if( isset($_SESSION['user_token']) ) :
						$title = "Geral";
					else :
						$title = "Login";						
					endif;

					break;
				case isset($_GET['edit']) && $_GET['edit'] =='item':
					$title = "Editar produto";	
					break;	
				case isset($_GET['stock']):
					$title = "Estoque";
					break;	
				case isset($_GET['users']):
					$title = "Usuários";
					break;	
				case isset($_GET['balance']):
					$title = "Balanço";	
					break;
				case isset($_GET['clients']):
					$title = "Clientes";	
					break;
				case isset($_GET['profile']):
					$title = "Perfil";	
					break;				
				case isset($_GET['sell']):
					$title = "Vender";	
					break;
				case isset($_GET['edit']) && $_GET['edit'] =='user':
					$title = "Editar usuário";	
					break;	
				case isset($_GET['edit']) && $_GET['edit'] =='client':
					$title = "Editar cliente";	
					break;	
			}
			// var_dump($title);
			return $title;
		}
	}
?>

